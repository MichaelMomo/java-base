package com.dongmo;

import java.util.Arrays;

/**
 * 
 * @author momog
 *
 */
public class Main {

	public static void main(String[] args) {
		// Arrays
		int value = 23;
		String name =  "Michael";
		
		// First declaration method. We created an initialised array
        int[] numbers = {120,67, 12, 23 , 67};
        
        // Second declaration method setting the length . We initialised the array after creation 
		int[] numbers1 = new int[3];
        
        // We can access to the single element of array
        System.out.println("The element at index 4 is " + numbers[4]);
        
        // We can set the value of the element at index i
		numbers1[0] = 100;
		numbers1[1] = 200;
		numbers1[2] = 300;
		
		// We can convert array to String: Arrays.toString()
		String array  = Arrays.toString(numbers);
		
		// Unsort Array
		
		System.out.println("Array elements: " + array ); 
		
		// Sort the elements of array
		Arrays.sort(numbers);
		
		System.out.println("Array elements after: "  + Arrays.toString(numbers)); 
		
		// Retrieve the length of an array "numbers.length"
		System.out.println(numbers.length);
	}

}
