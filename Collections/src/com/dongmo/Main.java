package com.dongmo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Consumer;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> l =  new ArrayList<String>();
		List<String> list =  new ArrayList<String>();
		List<String> li =  new ArrayList<String>();
		List<List<String>> l1 =  new ArrayList<>();
		
		// add elements in the list
		l.add("Michael");
		l.add("Cynthia");
		l.set(0, "Elton John");
		
		list.add("Dorince");
		list.add("Cynthia");
		list.add("Anna smith");
		list.add("Matteo roosi");
		
		li.addAll(list);
		li.addAll(l);
		
		l1.add(0, l);
		l1.add(1, list);
		l1.add(2, li);
		
		// get the size of list
		int dim = l.size();
		System.out.println(dim);
		
		// check if the list is empty
		System.out.println(l.isEmpty());
		
		// check if the list contains an particular element
		System.out.println(l.contains("Dorince"));
		
		// remove an element
		System.out.println(l.remove(0));
		
		// check if list A contains elements of list B
		System.out.println(list.containsAll(l));
		
		System.out.println(li);
		
		System.out.println(l.equals(li));
		
		System.out.println(li.iterator().next());
		
		// forEach collection 
		System.out.println("\n\n");
		li.sort(null);
		li.forEach(System.out::println);
		
		li.forEach(name -> System.out.println(name));
		
		
		// MAP <K, V>
		Map<Integer, String> map = new HashMap<Integer, String>();
		
		Map<Integer, String> m = new TreeMap<Integer, String>();
		
		m.put(234, "Fo'oh Dong");
		m.put(50, "Kana II");
		m.put(15, "Foto");
		m.put(30, "Dschang");
		m.put(27, "Bafou");
		
		// Add element in the map
		
		map.put(0, "Blondel");
		map.put(1, "Cynthia");
		map.put(2, "Dorince");
		map.put(3, "Imle");
		
		// get an element from map
		System.out.println("Corrisponding element to key 0 is " + " " + map.get(0));
		System.out.println("\n\n");
		// Traversing through the HashMap
		for(Map.Entry<Integer, String> elm : map.entrySet())
			System.out.println(elm.getKey() + " " + elm.getValue());
		
		System.out.println("\n\n");
		for(Map.Entry<Integer, String> elm : m.entrySet())
			System.out.println(elm.getKey() + " " + elm.getValue());
		
	}

}
