package com.dongmo;

public class Example {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Person person = new Person(
				"Dorince",
				"Smith",
				18
				);
		
		Person person1 = new Person();
		
		person1.setFirst_name("Dongmo");
		person1.setLast_name("Michael");
		person1.setAge(18);
		
		//--------------------------------------- Inheritance
		
		Teacher t =  new Teacher();
		
		t.setFirst_name("Dorince");
		t.setLast_name("Imle");
		t.setEmail("dorince@pippo.it");
		t.setPhone("+393489765432");
		t.setAge(45);
		t.setOffice("M34");
		t.setSalary(123456);
		t.setStart_year("2000");
		
		//--------------------------------------- Polimorphism
		
		Chien chien = new Chien();
		Chat chat = new Chat();
		
		String raceChien =  chien.name();
		String raceChat =  chat.name();
		
		System.out.println(raceChien);
		System.out.println(raceChat);
		
		System.out.println(t);
		
		System.out.println(person1);
		
		System.out.println(person);
	}

}
