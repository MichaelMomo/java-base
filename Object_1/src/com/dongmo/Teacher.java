package com.dongmo;

public class Teacher extends Persons {
	private int salary;
	private String office;
	private String start_year;
	
	public Teacher(String first_name, String last_name, String email, String phone, int age, int salary, String office,
			String start_year) {
		super(first_name, last_name, email, phone, age);
		this.salary = salary;
		this.office = office;
		this.start_year = start_year;
	}
	
	public Teacher() {
		super();
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getOffice() {
		return office;
	}

	public void setOffice(String office) {
		this.office = office;
	}

	public String getStart_year() {
		return start_year;
	}

	public void setStart_year(String start_year) {
		this.start_year = start_year;
	}

	@Override
	public String toString() {
		return "Teacher [salary=" + salary + ", office=" + office + ", start_year=" + start_year + "]";
	}
}
