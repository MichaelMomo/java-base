package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// Comparison Operators
		int cynthiaAge = 18;
		int dorinceAge = 23;
		
		String name1 = "Cynthia";
		String name2 = "Dorince";
		String email =  "dorince@unife.edu.it";
		
		boolean a = cynthiaAge < dorinceAge;
		
		System.out.println(a);
		System.out.println(cynthiaAge > dorinceAge);
		System.out.println(cynthiaAge <= dorinceAge);
		System.out.println(cynthiaAge >= dorinceAge);
		System.out.println(cynthiaAge == dorinceAge);
		System.out.println(cynthiaAge != dorinceAge);
		
		// String comparison
		System.out.println("\nString comparison --->\n");
		System.out.println(name1 == name2);
		System.out.println(name1.equals(name2));// Risk: if one of the string value is null the application returns NullPointerException
		
		System.out.println(name1.length() == name2.length());
		System.out.println(email.contains("@"));
	}

}
