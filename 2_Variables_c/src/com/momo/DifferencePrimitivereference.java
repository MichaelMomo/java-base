package com.momo;

public class DifferencePrimitivereference {
	
	public static void main(String[] args) {
		int dorince = 10;
		int synthia = dorince; // this value is passed by copy
		dorince = 100; // if we change the value of a, b can not change

		Person jordan = new Person("Jordan", 34);
		Person _dorince = jordan;
		Person _cynthia = _dorince;
		
		
		

		System.out.println("Dorince name= " + _dorince.name + " " + "Dorince age = " + _dorince.age);
		System.out.println("Cynthia name= " + _cynthia.name + " " + "Cynthia age = " + _cynthia.age);

		// change Dorince data
		_dorince.name = "John Smith";
		_dorince.age = 34;

		System.out.println("\n Persons dta after modifications\n");

		System.out.println("Dorince name= " + _dorince.name + " " + "Dorince age = " + _dorince.age);
		System.out.println("Cynthia name= " + _cynthia.name + " " + "Cynthia age = " + _cynthia.age);

		_cynthia.name = "Anna Smith";
		_cynthia.age = 30;

		System.out.println("\n Persons dta after changing Cynthia data\n");

		System.out.println("Dorince name= " + _dorince.name + " " + "Dorince age = " + _dorince.age);
		System.out.println("Cynthia name= " + _cynthia.name + " " + "Cynthia age = " + _cynthia.age);
		System.out.println("jordan name= " + jordan.name + " " + "jordan age = " + jordan.age);

//		System.out.println("dorince " + dorince);
//		System.out.println("synthia " + synthia);
//		
//		System.out.println("dorince after change " + dorince);
//		System.out.println("synthia after change " + synthia);

	}

	static class Person {
		String name;
		int age;

		Person(String name, int age) {
			this.name = name;
			this.age = age;
		}
	}

}
