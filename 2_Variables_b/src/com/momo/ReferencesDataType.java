package com.momo;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

public class ReferencesDataType {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		// Non primitive data types AKA reference types
		
		String paccaociaName = "Paccocia";
		
		String cynthiaName = paccaociaName;
		
//		System.out.println("Paccocia name is " + paccaociaName);
//		
//		System.out.println("Cynthia name is " + cynthiaName);
//		
//		paccaociaName = "Adam Smith";
//		
//		System.out.println("Paccocia name after change is " + paccaociaName);
//		
//		System.out.println("Cynthia name after change is " + cynthiaName);
//		
//		cynthiaName = "Anna";
//		
//		System.out.println("Paccocia name after change is " + paccaociaName);
//		
//		System.out.println("Cynthia name after change is " + cynthiaName);
		
		Date date = new Date();
		System.out.println("\nThe current date is "  + date);
		System.out.println("\nThe current hour is "  + date.getHours());
		System.out.println("\nThe current minute is "  + date.getMinutes());
		
		LocalDateTime time = LocalDateTime.now();
		System.out.println("The current time is" + " " + time);
		
		LocalDate now = LocalDate.now();
		
		System.out.println("\nThe current date is" + " " + now);
		System.out.println("\nThe current day is" + " " + now.getDayOfMonth());
		System.out.println("\nThe current month is " + " " + now.getMonthValue());
		System.out.println("\nThe current year is" + " " + now.getYear());
		
	}

}
