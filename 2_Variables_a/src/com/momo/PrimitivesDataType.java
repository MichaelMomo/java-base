package com.momo;

public class PrimitivesDataType {

	public static void main(String[] args) {
		//this allows you to store all number
		byte theBythe =  -128;
		
		short theShort = 8989;
		
		int theInt =  7878; 
		
		long theLong = 7878787878787878L;
		
		//This allows you to store decimal number
		float theFloat = 3.14F; // this is sufficient to store six to seven decimals digits
		double theDouble = 3.1415; // this is sufficient to store 15 decimals digits
		
		// this is to store boolean
		boolean theBoolean = true;
		
		//the char for store single character
		char initCharacter = 'A';
		
		System.out.println();
		
		System.out.println("theBythe " + theBythe);
		System.out.println("theShort " + theShort);
		System.out.println("theInt " + theInt);
		System.out.println("theLong " + theLong);
		System.out.println("theFloat " + theFloat);
		System.out.println("theDouble " + theDouble);
		System.out.println("theBoolean " + theBoolean);
		System.out.println("initCharacter " + initCharacter);

	}

}
