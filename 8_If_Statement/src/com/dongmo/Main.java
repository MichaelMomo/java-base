package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// if statements
		int age = 17;
		
//		if (age < 18) {
//			System.out.println("Ouuuuuufff y're not an adult! (: ");
//		} else {
//			System.out.println("Ouuuuraaa your are an adult :) ");
//		}
		
		// multiple if instruction
		if (age >= 18) {
			System.out.println("Ouuuuuufff y're not an adult! (: ");
		} else if (age == 17) {
			System.out.println("Ouups!! y're to be an adult  ... ");
		} else if (age == 18) {
			System.out.println("Yeeeh y're an adult   :)");
		}else {
			System.out.println("Your age is more than 18 years...");
		}
		
	}

}
