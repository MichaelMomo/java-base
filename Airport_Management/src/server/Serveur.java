package server;

import java.util.Scanner;

public class Serveur {

	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Hi! please insert your first name");
		String name = scan.nextLine();
		
		System.out.println("Hi! please insert your role");
		String role = scan.nextLine();
		
		switch (role.toUpperCase()) {
		
		case "PASSENGER": 
			System.out.println("Welcome Passenger " + name.toUpperCase() + "Y're a passenger");
			break;
			
		case "CHECK_IN_AGENT": 
			System.out.println("Welcome Ckeck-in agent " + name.toUpperCase() + " Y're a CHECK_IN_AGENT");
			break;
			
		case "INFORMATION_AGENT": 
			System.out.println("Welcome information agent " + name.toUpperCase() + "Y're a INFORMATION_AGENT");
			break;
			
		case "ADMINISTRATOR": 
			System.out.println("https://www.javatpoint.com/java-oops-concepts" + name.toUpperCase() + "Y're a ADMINISTRATOR");
			break;
		
		default:
			throw new IllegalArgumentException("Unauthorized user!!" );
		}
	}
}
