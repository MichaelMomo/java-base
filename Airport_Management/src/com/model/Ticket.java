package com.model;

public class Ticket extends Users {
	
	private String ticket_id;
	private String seat_number;
	private String travel_type;
	private String fly_class;
	private int baggage;
	private Fly fly;
	
	public Ticket(String first_name, String last_name, String address, String city, String birth, Gender gender,
			String ticket_id, String seat_number, String travel_type, String fly_class, Fly fly) {
		super(first_name, last_name, address, city, birth, gender);
		this.ticket_id = ticket_id;
		this.seat_number = seat_number;
		this.travel_type = travel_type;
		this.fly_class = fly_class;
		this.fly = fly;
	}
	
	public Ticket() {}

	public String getTicket_id() {
		return ticket_id;
	}

	public void setTicket_id(String ticket_id) {
		this.ticket_id = ticket_id;
	}

	public String getSeat_number() {
		return seat_number;
	}

	public void setSeat_number(String seat_number) {
		this.seat_number = seat_number;
	}

	public String getTravel_type() {
		return travel_type;
	}

	public void setTravel_type(String travel_type) {
		this.travel_type = travel_type;
	}

	public String getFly_class() {
		return fly_class;
	}

	public void setFly_class(String fly_class) {
		this.fly_class = fly_class;
	}

	public Fly getFly() {
		return fly;
	}

	public void setFly(Fly fly) {
		this.fly = fly;
	}

	@Override
	public String toString() {
		return "Ticket [ticket_id=" + ticket_id + ", seat_number=" + seat_number + ", travel_type=" + travel_type
				+ ", fly_class=" + fly_class + ", fly=" + fly + ", getFirst_name()=" + getFirst_name()
				+ ", getLast_name()=" + getLast_name() + ", getAddress()=" + getAddress() + ", getCity()=" + getCity()
				+ ", getBirth()=" + getBirth() + ", getGender()=" + getGender() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
