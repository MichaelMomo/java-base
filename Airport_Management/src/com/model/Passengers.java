package com.model;

public class Passengers extends Persons {
	private Documents document;
	private Baggage baggage;
	
	public Passengers(String first_name, String last_name, String address, String city, String birth, Gender gender,
			String phone_number, String email, String role, Documents document, Baggage baggage) {
		super(first_name, last_name, address, city, birth, gender, phone_number, email, role);
		this.document = document;
		this.baggage = baggage;
	}
	
	public Passengers(){}

	public Documents getDocument() {
		return document;
	}

	public void setDocument(Documents document) {
		this.document = document;
	}

	public Baggage getBaggage() {
		return baggage;
	}

	public void setBaggage(Baggage baggage) {
		this.baggage = baggage;
	}

	@Override
	public String toString() {
		return "Passengers [document=" + document + ", baggage=" + baggage + ", getPhone_number()=" + getPhone_number()
				+ ", getEmail()=" + getEmail() + ", getRole()=" + getRole() + ", toString()=" + super.toString()
				+ ", getFirst_name()=" + getFirst_name() + ", getLast_name()=" + getLast_name() + ", getAddress()="
				+ getAddress() + ", getCity()=" + getCity() + ", getBirth()=" + getBirth() + ", getGender()="
				+ getGender() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
}
