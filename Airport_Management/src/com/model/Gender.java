package com.model;

public enum Gender {
	
	MALE,
	FEMALE,
	PREFER_NOT_TO_SAY
	
}
