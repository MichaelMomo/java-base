package com.model;

public class Persons extends Users {
	
	private String phone_number;
	private String email;
	private String role;
	
	public Persons(String first_name, String last_name, String address, String city, String birth, Gender gender,
			String phone_number, String email, String role) {
		super(first_name, last_name, address, city, birth, gender);
		this.phone_number = phone_number;
		this.email = email;
		this.role = role;
	}
	
	public Persons() {}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public String toString() {
		return "Persons [phone_number=" + phone_number + ", email=" + email + ", role=" + role + ", getFirst_name()="
				+ getFirst_name() + ", getLast_name()=" + getLast_name() + ", getAddress()=" + getAddress()
				+ ", getCity()=" + getCity() + ", getBirth()=" + getBirth() + ", getGender()=" + getGender()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
}
