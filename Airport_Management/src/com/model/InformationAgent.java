package com.model;

public class InformationAgent extends Persons {
	
	private String agent_id;
	private String hire_date;
	
	public InformationAgent(String first_name, String last_name, String address, String city, String birth,
			Gender gender, String phone_number, String email, String role, String agent_id, String hire_date) {
		super(first_name, last_name, address, city, birth, gender, phone_number, email, role);
		this.agent_id = agent_id;
		this.hire_date = hire_date;
	}
	
	public InformationAgent() {}

	public String getAgent_id() {
		return agent_id;
	}

	public void setAgent_id(String agent_id) {
		this.agent_id = agent_id;
	}

	public String getHire_date() {
		return hire_date;
	}

	public void setHire_date(String hire_date) {
		this.hire_date = hire_date;
	}

	@Override
	public String toString() {
		return "InformationAgent [agent_id=" + agent_id + ", hire_date=" + hire_date + ", getPhone_number()="
				+ getPhone_number() + ", getEmail()=" + getEmail() + ", getRole()=" + getRole() + ", toString()="
				+ super.toString() + ", getFirst_name()=" + getFirst_name() + ", getLast_name()=" + getLast_name()
				+ ", getAddress()=" + getAddress() + ", getCity()=" + getCity() + ", getBirth()=" + getBirth()
				+ ", getGender()=" + getGender() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}	
}
