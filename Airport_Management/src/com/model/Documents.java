package com.model;

public class Documents extends Users {
	
	private String number;
	private String deliver_date;
	private String expire_date;
	private String authority;
	private String job;
	private String type;

	public Documents(String first_name, String last_name, String address, String city, String birth, Gender gender,
			String number, String deliver_date, String expire_date, String authority, String job, String type) {
		super(first_name, last_name, address, city, birth, gender);
		this.number = number;
		this.deliver_date = deliver_date;
		this.expire_date = expire_date;
		this.authority = authority;
		this.job = job;
		this.type = type;
	}

	public Documents() {}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDeliver_date() {
		return deliver_date;
	}

	public void setDeliver_date(String deliver_date) {
		this.deliver_date = deliver_date;
	}

	public String getExpire_date() {
		return expire_date;
	}

	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Documents [number=" + number + ", deliver_date=" + deliver_date + ", expire_date=" + expire_date
				+ ", authority=" + authority + ", job=" + job + ", type=" + type + ", getFirst_name()="
				+ getFirst_name() + ", getLast_name()=" + getLast_name() + ", getAddress()=" + getAddress()
				+ ", getCity()=" + getCity() + ", getBirth()=" + getBirth() + ", getGender()=" + getGender()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}		
}
