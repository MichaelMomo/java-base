package com.model;

import java.time.LocalDateTime;

public class Fly {
	
	private String fly_id;
	private String gate;
	private String city_from;
	private String destination;
	private LocalDateTime departureTime;
	private LocalDateTime arrival;
	private long duration;
	
	public Fly(String fly_id, String gate, String city_from, String destination, LocalDateTime departureTime,
			LocalDateTime arrival, long duration) {
		super();
		this.fly_id = fly_id;
		this.gate = gate;
		this.city_from = city_from;
		this.destination = destination;
		this.departureTime = departureTime;
		this.arrival = arrival;
		this.duration = duration;
	}
	
	public Fly() {}

	public String getFly_id() {
		return fly_id;
	}

	public void setFly_id(String fly_id) {
		this.fly_id = fly_id;
	}

	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getCity_from() {
		return city_from;
	}

	public void setCity_from(String city_from) {
		this.city_from = city_from;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public LocalDateTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(LocalDateTime departureTime) {
		this.departureTime = departureTime;
	}

	public LocalDateTime getArrival() {
		return arrival;
	}

	public void setArrival(LocalDateTime arrival) {
		this.arrival = arrival;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	@Override
	public String toString() {
		return "Fly [fly_id=" + fly_id + ", gate=" + gate + ", city_from=" + city_from + ", destination=" + destination
				+ ", departureTime=" + departureTime + ", arrival=" + arrival + ", duration=" + duration + "]";
	}
}
