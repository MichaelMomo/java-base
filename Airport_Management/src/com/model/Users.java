package com.model;

public class Users {
	
	private String first_name;
	private String last_name;
	private String address;
	private String city;
	private String birth;
	private Gender gender;
	
	public Users(String first_name, String last_name, String address, String city, String birth, Gender gender) {
		super();
		this.first_name = first_name;
		this.last_name = last_name;
		this.address = address;
		this.city = city;
		this.birth = birth;
		this.gender = gender;
	}
	
	public Users() {}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Users [first_name=" + first_name + ", last_name=" + last_name + ", address=" + address + ", city="
				+ city + ", birth=" + birth + ", gender=" + gender + "]";
	}

}
