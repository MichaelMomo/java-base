package com.model;

public class Baggage {
	
	private String dimensions;
	private float weight;
	private String bagg_id;
	
	public Baggage(String dimensions, float weight, String bagg_id) {
		super();
		this.dimensions = dimensions;
		this.weight = weight;
		this.bagg_id = bagg_id;
	}
	
	public Baggage() {}

	public String getDimensions() {
		return dimensions;
	}

	public void setDimensions(String dimensions) {
		this.dimensions = dimensions;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getBagg_id() {
		return bagg_id;
	}

	public void setBagg_id(String bagg_id) {
		this.bagg_id = bagg_id;
	}

	@Override
	public String toString() {
		return "Baggage [dimensions=" + dimensions + ", weight=" + weight + ", bagg_id=" + bagg_id + "]";
	}
}
