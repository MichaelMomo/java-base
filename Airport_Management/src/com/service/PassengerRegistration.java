package com.service;

import java.util.ArrayList;
import java.util.List;

import com.model.Passengers;

public class PassengerRegistration {
	
	public String save(Passengers passengers) {
		
		// create a list that will contains all passengers
		List<Passengers> passenger =  new ArrayList<>();
		
		// Create a new passenger object
		Passengers traveler =  new Passengers();
		
		// Set passenger properties
		traveler.setFirst_name(passengers.getFirst_name());
		traveler.setLast_name(passengers.getLast_name());
		traveler.setBirth(passengers.getBirth());
		traveler.setAddress(passengers.getAddress());
		traveler.setCity(passengers.getCity());
		traveler.setEmail(passengers.getEmail());
		traveler.setPhone_number(passengers.getPhone_number());
		traveler.setRole(passengers.getRole());
		traveler.setDocument(passengers.getDocument());
		traveler.setBaggage(passengers.getBaggage());
		
				
		// add the new passenger in our list(save  a new passenger)
		passenger.add(traveler);
		
		return "Passeger registred succefull";
	}

}
