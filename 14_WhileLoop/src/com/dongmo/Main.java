package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// While loop
		int count = 0;
//		while (count <= 20) {
//			System.out.println("count " + count);
//			count++;
//		}
		
		do {
			System.out.println("count " + count);
			count++;
		} while (count <= 20);

	}

}
