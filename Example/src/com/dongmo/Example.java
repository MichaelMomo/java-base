package com.dongmo;

import java.time.LocalDate;
import java.util.Scanner;

public class Example {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scan =  new Scanner(System.in);
		Person user = new Person();
		
		System.out.println("Hi, what is your first name ? ");
		String username =  scan.nextLine();
		user.setFirst_name(username);
		
		System.out.println("What is your last name ? ");
		String lastname =  scan.nextLine();
		user.setLast_name(lastname);
		
		System.out.println("What is your age ? ");
		int age = scan.nextInt();
		user.setAge(age);
		
		int year  =  LocalDate.now().minusYears(age).getYear();
		
		if (age >= 18) {
			System.out.println("Welcome " + username + " "+ lastname + " " + "and you were born in " + year);
		}else {
			System.out.println("Welcome " + username + " " + lastname + " " + "you are not an adult and you were born in " + year);
		}
	}

}
