package com.dongmo;

import java.util.Arrays;

public class Main {

	public static void main(String[] args) {
		// Loops
		
		int[] numbers =  {23, 37, 40, 48, 50, 78, 98, 100, 89, 98, 567};
		
		// For ASC
		for (int i = 0; i < numbers.length ; i++) {
			
			// numbers[i] +=  1;
			
			if (numbers[i] == 98) {
				System.out.println(numbers[i]);
				System.out.println("Position = " + i);
			}
			
			//System.out.println(numbers[i]);
		}
		
		
		// FOR DESC
//		for (int i = numbers.length-1; i >= 0; i--) {
//			System.out.println(numbers[i]);
//		}
		
//		int number = 0;
//		number++;
//		
//		System.out.println(number);
		
		// ENHANCED FOR LOOP
		
		for (int number : numbers) {
			System.out.println(number);
		}
		
		
		// BONUS
		Arrays.stream(numbers).forEach(System.out::println);
	}

}
