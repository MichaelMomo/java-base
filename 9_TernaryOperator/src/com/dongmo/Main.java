package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// Ternary Operator
		int age = 18;
		
		String message = age >= 18 ? "Hooooray.... I am adult" : "I am not an adult";

		// equivalent
		
		if (age >= 18) {
			System.out.println("Hooooray.... I am adult! (: ");
		} else {
			System.out.println("I am not an adult :) ");
		}

		System.out.println(message);
	}

}
