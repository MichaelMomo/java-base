package com.dongmo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/* Funzione ricorsiva che dato un numero i >= 0 calcola il numero di Fibonacci
i-esimo:

L'i-esimo numero di Fibonacci F(i) e` definito attraverso la seguente
definizione induttiva:

  F(i) = 0                 se i = 0
  F(i) = 1                 se i = 1
  F(i) = F(i-1) + F(i-2)   se i >= 2
*/
public class Fibonacci {
	
/* Calcola l'i-esimo numero di Fibonacci.
  Si utilizza il tipo long in quanto i numeri di Fibonacci crescono molto
  velocemente (analogamente all'esponenziale).
 */

 public static long fibonacci(long i) {
	/* F(i) non e` definito per interi i negativi! */
	if (i == 0) return 0;
	else if (i == 1) return 1;
	else return fibonacci(i-1) + fibonacci(i-2);
 }

 public static void main(String[] args) throws IOException {
	BufferedReader br = new BufferedReader(
				new InputStreamReader(System.in));
	long i;
	System.out.print("Inserire un intero i >= 0: ");
	i = Integer.parseInt(br.readLine());
	System.out.print("fibonacci(" + i + ") = " + fibonacci(i));
 }
}
