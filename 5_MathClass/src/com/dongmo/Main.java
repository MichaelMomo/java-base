package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// Math
		
		// positive number
		System.out.println("Positive number " + Math.abs(-10));
		// Maximum
		System.out.println("Max between two numbers " + Math.max(3.6, 10));
		// minimum
		System.out.println("Min between two numbers " + Math.min(3.6, 10));
		//power
		System.out.println("Power " + Math.pow(5.0, 2.0));
		//Square root 
		System.out.println("Square root of number " + Math.sqrt(25));
		// Pi
		System.out.println("PI Value " + Math.PI);
		// Sum 
		System.out.println("The Exact sum " + Math.addExact(2, 3));
		// Decrement by one
		System.out.println("Decrement by one " + Math.decrementExact(10));
		// Random
		System.out.println("Random number " + Math.random());
	}

}
