package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// Switch Statements
		String name = "Elon";

		switch (name) {
		case "Dorince":
			System.out.println("Welcome " + name);
			break;
		case "Cynthia":
			System.out.println("Welcome " + name);
			break;
		case "Michael":
			System.out.println("Welcome " + name);
			break;
		case "Anna Smith":
			System.out.println("Welcome " + name);
			break;
		default:
			System.out.println("Unknown name !!!");
		}
	}
}
