package com.dongmo;

public class Recurtion {
	
	static int factorial(int value) {
		
		if (value == 1) {
			return 1;
		}else {  
			return (value * factorial(value-1));
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int val = 6;
		
		System.out.println("Fattoriale di " + val + " =  " + factorial(val));
		
	}
}
