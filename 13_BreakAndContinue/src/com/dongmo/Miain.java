package com.dongmo;

public class Miain {

	public static void main(String[] args) {
		// Break and Continue
		String[] names = {"Cynthia", "Rodrigue", "Pakocia", "Dorince" };
		
		System.out.println("break ------>\n");
		
		for (String name : names) {
			if (name.equals("Pakocia")) {
				break;
			}
			System.out.println(name);
		}
		
		System.out.println("\ncontinue ------>\n");
		
		for (String name : names) {
			if (name.startsWith("R") || name.startsWith("D")) {
				continue;
			}
			System.out.println(name);
		}

	}

}
