package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// Logical Operators
		boolean isEnabled = true;
		boolean isAdult = true;
		boolean isStudent = false;
		boolean isAfterSchoolMember = false;
		
		// all the expression must be true: &&
		if (isAdult && isStudent && isAfterSchoolMember) {
			System.out.println("Yeeeehhh y're are an adult!!! :) ");
		} else {
			System.out.println("Y're not an adult (: ");
		}
		
		 // one of the expression must be true: &&
		boolean result = isAdult || isStudent || isAfterSchoolMember;
		
		if (result) {
			System.out.println("Yeeeehhh y're are an adult!!! :) ");
		} else {
			System.out.println("Y're not an adult (: ");
		}
		
		// Combine logical operator
		
		if (isEnabled && 
				isAdult || 
				isStudent || 
				isAfterSchoolMember
				) {
			System.out.println("Yeeeehhh y're are an adult!!! :) ");
		}else {
			System.out.println("Y're not an adult (: ");
		}
	}

}
