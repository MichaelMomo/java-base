package com.dongmo;

import java.util.Iterator;

/**
 * 
 * @author momog
 *
 */
public class Main {

	/**
	 * Let create our first method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		// Methods
		char[] letters = {'A', 'A', 'B', 'C', 'D', 'D', 'D'};
		
		// invoke our method
		int count = countOccurrences(letters, 'B');
		System.out.println(count);
	}
	
	/**
	 * This method will return the number of occurrence of any giving letter
	 * public: means this method can be accessed by oder classes 
	 * static: this methods beyond to a class 
	 *  
	 * @param letters
	 * @param searchLetter
	 * @return count
	 */
	public static int countOccurrences(char[] letters, char searchLetter) {
		
		int count = 0;
		for (int i = 0; i < letters.length; i++) {
			if (letters[i]== searchLetter) {
				count += 1;
			}
		}
		return count;
	}

}
