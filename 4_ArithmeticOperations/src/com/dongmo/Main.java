package com.dongmo;

public class Main {

	public static void main(String[] args) {
		// Arithmetic operations 
		
		int number1 =  13;
		int number2 =  10;
		
		// sum a + b
		int sumResult =  number1 + number2;
		
		// Product a * b
		int productResult =  number1 * number2;
		
		// Difference
		int diffResult = number1 - number2;
		
		// Module
		int modResult =  13%10;
		
		System.out.println("number1 + number2 = " + sumResult);
		System.out.println("number1 * number2 = " + productResult);
		System.out.println("number1 - number2 = " + diffResult);
		System.out.println("number1 % number2 = " + modResult);
		
		// NB: There is not a sum between String but there is is a concatenation between String
		
		String name1 = "Guimeya";
		String name2 = "Cynthia";
		String name3 = name1 + " " + name2;
		System.out.println(name3);
		
		//Bodmas(Body multiplication addition subtract)
		System.out.println(10 * (2 + 7) - 10);
	}

}
