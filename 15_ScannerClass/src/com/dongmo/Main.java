package com.dongmo;

import java.time.LocalDate;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// Scanner
		Scanner scanner =  new Scanner(System.in);
		
		System.out.println("What is your name ?");
		
		String userName = scanner.nextLine();
		System.out.println("\nHello " + userName);
		
		System.out.println("How old are you ?");
		int age = scanner.nextInt();
		int year = LocalDate.now().minusYears(age).getYear();
		if (age >= 18) {
			System.out.println("You were born in " + year + " "+ "and you are an adult"+ " :)");
		}else {
			System.out.println("You were born in " + year + " "+ "and you are not an adult"+ " (: ");
		}
	}

}
