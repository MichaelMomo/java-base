package com.dongmo;

//Java program to demonstrate 
//insert operation in binary
//search tree
class BinarySearchTree {

	// create a node containing left and right child of current node
	class Node {
		int val; // Root ----> racine
		Node left; // left child
		Node right; // right child

		// constructor
		public Node(int val) {
			this.val = val;
			this.left = this.right = null;
		}
	}
	
	// Root of BST
	 Node root;
	 
	 // method to call inserRec in the main met
	 void insert(int key) {
		 inserRec(root, key);
	 }

	// recursive function to insert a new key
	Node inserRec(Node root, int key) {

		/*
		 * if the tree is empty return a new node
		 */
		if (root == null) {
			root = new Node(key);
			return root;
		}

		/**
		 * Otherwise, recur down the tree
		 */
		if (key < root.val) {
			root.left = inserRec(root.left, key);
		} else if (key > root.val) {
			root.right = inserRec(root.right, key);
		}

		/*
		 * return the node 
		 */
		return root;
	}
	
	// Method to call inOrderRec in main
	void inOrder() {
		inOrderRec(root);
	}
	
	// function to do inOrder traversal
	void inOrderRec(Node root) {
		if (root != null) {
			inOrderRec(root.left);
			System.out.println(root.val);
			inOrderRec(root.right);
		}	
	}

	
	// Driver Code
	public static void main(String[] args) {
		BinarySearchTree tree = new BinarySearchTree();

		/*
		 * Let us create following BST 
		 * 
		 *    50 
		 *   /  \ 
		 * 30    70 
		 * / \    / \ 
		 * 20 40 60 80
		 */
		tree.insert(50);
		tree.insert(30);
		tree.insert(20);
		tree.insert(40);
		tree.insert(70);
		tree.insert(60);
		tree.insert(80);

		// print inOrder traversal of the BST
		tree.inOrder();
	}
}
