package com.dongmo;

/**
 * Methods is a bloc of code which is only run when it's called to perform a logic. It can take parameters. 
 * In java there are many classes which comes with a bunch of methods that we can use. e.g: String class
 * 
 * @author momog
 *
 */

public class Main {

	public static void main(String[] args) {
		// Methods
		System.out.println("Hello"); // println() is a method and it takes parameter in this case a String
		
		String brand =  "Samsung";
		System.out.println(brand.toUpperCase()); // Call method to perform a logic

	}

}
