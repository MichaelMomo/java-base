package com.momo;

public class Main {

	public static void main(String[] args) {
		// String
		
		String name = new String("Michael"); // Strings are immutable. 
		
		String cynthia = "Cynthia";
		
		System.out.println(cynthia.startsWith("C"));
		
		System.out.println(cynthia.endsWith("o"));
		
		System.out.println(cynthia.toUpperCase());
		
		System.out.println(cynthia.indexOf("n"));
		
		// split
		String frase = "I am learnin java Program - I start from scratch.- I am happy to learn java"; 
		
		// you can split a chain of characters. 
		String[] _split = frase.split("-");
		
		System.out.println(_split[0]);
		System.out.println(_split[1]);
		System.out.println(_split[2]);
		
		StringBuffer sb = new StringBuffer(name); // to create mutable string
		
		System.out.println(name);
		
		//Convert all characters of my string in uppercase
		System.out.println(name.toUpperCase());
		
		//Convert all characters of my string in lowercase
		System.out.println(name.toLowerCase());
		
		//access a specific character in particular position
		System.out.println(name.charAt(0));
		
		//Check is a chain of character contain a specific word o character 
		System.out.println(name.contains("chael"));
		
		System.out.println(name.contains("kappa"));
		
		// convert a number to String 
		int number = 15;
		String numberConvert = String.valueOf(number);
		System.out.println(numberConvert);
		//convert a String to number
		int numero = Integer.parseInt(numberConvert);
		System.out.println(numero);
	}

}
