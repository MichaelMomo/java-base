package com.dongmo;

import java.time.LocalDate;

/**
 * The class is a blueprint to create anything that you want.
 * Object are anything that you can think in the real world.
 * In software anything are class and object
 * 
 * 
 * @author momog
 *
 */
public class Main {

	public static void main(String[] args) {
		// Classes and Objects
		IdCard card =  new IdCard("CA7643WS87", "DONGMO MICHAEL", LocalDate.of(2029, 11, 01), false); // this is how we create an object. card is an object
		System.out.println(card);
	}
	
	/**
	 * This is the class (blueprint) to create id card
	 * 
	 */
	static class IdCard {
		String idCardNumber;
		String ownerName;
		LocalDate expiredDate;
		boolean isAuthorisedForTransit;
		
		// let create a constructor to get an object of type idCard
		IdCard(String idCardNumber,
				String ownerName,
				LocalDate expiredDate,
				boolean isAuthorisedForTransit) {
			
			this.idCardNumber = idCardNumber;
			this.ownerName = ownerName;
			this.expiredDate = expiredDate;
			this.isAuthorisedForTransit =  isAuthorisedForTransit;
			
		}

		@Override
		public String toString() {
			return "IdCard [idCardNumber=" + idCardNumber + ", ownerName=" + ownerName + ", expiredDate=" + expiredDate
					+ ", isAuthorisedForTransit=" + isAuthorisedForTransit + "]";
		}
	}

}
